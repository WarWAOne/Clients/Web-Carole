import 'package:angular/angular.dart';
import 'package:carole_gosset/app_component.template.dart' as ng;

void main() {
  runApp(ng.AppComponentNgFactory);
}
