import 'package:angular/angular.dart';
import 'package:angular_forms/angular_forms.dart';
import 'package:application_library/components/App/App.dart';
import 'package:application_library/components/Toolbar/Toolbar.dart';
import 'package:carole_gosset/src/components/HomePage/HomePage.dart';
import 'package:carole_gosset/src/components/BiographyPage/BiographyPage.dart';

@Component(
  selector: 'app',
  templateUrl: 'template.html',
  styleUrls: ['style.css'],
  directives: [coreDirectives, formDirectives, App, ToolbarComponent, HomePage, Biography],
)
class AppComponent {

  static const HOME_PAGE = 'Acceuil';
  static const BIOGRAPHY_PAGE = 'Biography';
  static const ARTICLES_PAGE = 'Mes articles';
  static const CONTACT_PAGE = 'Me contacter';
  static const WORKS_PAGE = 'Mes oeuvres';

  final String title = "Carole Gosset";
  final String mainActionText = "Mes oeuvres";
  final String email = "carolegosset@gmail.com";
  final String developpers = "WarWAOne";
  final menu = [BIOGRAPHY_PAGE, ARTICLES_PAGE, CONTACT_PAGE];
  String actualPage = HOME_PAGE;

  void onMainAction(e) {
    print("click");
  }

  void onClickMenuItem(item) {
    actualPage = item;
  }
}
