import 'package:angular/angular.dart';

@Component( 
  selector: 'presentation',
  templateUrl: 'presentation_template.html',
  styleUrls: ['presentation_style.css'],
  directives: [coreDirectives]
)
class PresentationWidget {
  @Input()
  String title = 'My presentation';

  @Input()
  String description = 'Some description';

  @Input()
  String buttonText = 'somewhere';

  @Input()
  String imgUrl = '';
}