import 'package:angular/angular.dart';
import 'package:carole_gosset/src/components/PresentationWidget/PresentationWidget.dart';

@Component(
  selector: 'homepage',
  templateUrl: 'homepage_template.html',
  styleUrls: ['homepage_style.css'],
  directives: [coreDirectives, PresentationWidget],
)
class HomePage {
  final title = "Carole Gosset";
  final bio = "Biographie";
  final description = "lorem ipsum machin truc bidule desc ici la popi pola li ne lioknd bi tashu testu lina badu";
}