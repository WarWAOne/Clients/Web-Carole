import 'package:angular/angular.dart';

@Component(
  selector: 'biography',
  templateUrl: 'biography_template.html',
  styleUrls: ['biography_style.css'],
  directives: [coreDirectives],
)
class Biography {
  final title = 'Mon titre';
  final biography = 'Ma biography ici';
  final coverPicture = '';
}