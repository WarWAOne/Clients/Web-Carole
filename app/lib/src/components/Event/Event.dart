import 'package:angular/angular.dart';

@Component(
  selector: 'event',
  templateUrl: 'event_template.html',
  styleUrls: ['event_style.css'],
  directives: [coreDirectives],
)
class Event {
  @Input()
  String title = 'New event';

  @Input()
  String description = 'description of the event';

  @Input()
  String place = 'somewhere';

  @Input()
  String date = 'sometime';
}